package com.licious.merchandising.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.licious.merchandising.ControllerTestUtility;
import com.licious.merchandising.dtos.models.WidgetMappingDto;
import com.licious.merchandising.dtos.request.WidgetCreationRequestV1Dto;
import com.licious.merchandising.exceptions.InvalidParameterValueException;
import com.licious.merchandising.models.WidgetMapping;
import com.licious.merchandising.services.WidgetService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.contains;

@WebMvcTest(WidgetController.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class WidgetControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private WidgetService widgetService;

    private WidgetCreationRequestV1Dto updateDto;

    @BeforeAll
    void setup() {
        updateDto = new WidgetCreationRequestV1Dto();
        WidgetMappingDto mappingDto = new WidgetMappingDto();
        mappingDto.setId(1L);
        mappingDto.setAppImage("testImageURL");
        mappingDto.setAppUrl("testUrl");
        mappingDto.setWebImage("testImageURL");
        mappingDto.setWebUrl("testUrl");
        mappingDto.setMsiteImage("testImageURL");
        mappingDto.setCategoryId(1L);
        mappingDto.setCardName("cardName");
        mappingDto.setCardPosition(1);
        List<WidgetMappingDto> mappings = new ArrayList<>();
        mappings.add(mappingDto);

        updateDto.setId(1L);
        updateDto.setAndroidEnabled(true);
        updateDto.setIosEnabled(true);
        updateDto.setWebEnabled(true);
        updateDto.setMsiteEnabled(true);
        updateDto.setLayoutName("short1");
        updateDto.setLayoutAspectRatio("160x180");
        updateDto.setName("updatedName");
        updateDto.setType("COLLECTION");
        updateDto.setSubText("testSubText");
        updateDto.setStatus("ACTIVE");
        updateDto.setWidgetMappings(mappings);

        WidgetMapping widgetMapping = new WidgetMapping();
        widgetMapping.setId(1L);
        widgetMapping.setCardName("testCardName");
        widgetMapping.setCardPosition(1);
        widgetMapping.setAppImage("testImage");
        widgetMapping.setWebImage("testImage");
        widgetMapping.setMsiteImage("testImage");
        widgetMapping.setAppUrl("testAppUrl");
        widgetMapping.setWebUrl("testWebUrl");
        widgetMapping.setCategoryId(1L);

        Set<WidgetMapping> widgetMappings = new HashSet<>();
        widgetMappings.add(widgetMapping);

    }

    @Test
    void testInjection(){
        assertNotNull(mockMvc);
    }

    /**
     * Positive test case with proper request body
     * Expected 200 Ok
     */
    @Test
    public void testUpdateWidgetSuccess() throws Exception {
        long id = 1;
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = ow.writeValueAsString(updateDto);
        MockHttpServletRequestBuilder builder = ControllerTestUtility.patchBuilder("/api/v1/widgets/" + id , json);
        this.mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status()
                        .isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    /**
     * Negative test case with required parameters null
     * Expected 400 Bad Request
     */
    @Test
    public void testUpdateWidgetWithRequiredParametersNull() throws Exception {
        long id = 1;
        WidgetCreationRequestV1Dto dto = new WidgetCreationRequestV1Dto();
        dto.setName("sample");
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = ow.writeValueAsString(dto);
        MockHttpServletRequestBuilder builder = ControllerTestUtility.patchBuilder("/api/v1/widgets/" + id , json);
        this.mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status()
                        .isBadRequest())
                .andDo(MockMvcResultHandlers.print());
    }

    /**
     * Negative test case with updateWidget method throwing InvalidParameterValueException
     * Expected 400 Bad Request
     */
    @Test
    public void testUpdateWidgetWithInvalidParameterValueExceptionFromService() throws Exception {
        long id = 1;
        Mockito.when(widgetService.updateWidget(any(), any())).thenThrow(new InvalidParameterValueException("Sample Error"));
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = ow.writeValueAsString(updateDto);
        MockHttpServletRequestBuilder builder = ControllerTestUtility.patchBuilder("/api/v1/widgets/" + id , json);
        MvcResult result = this.mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status()
                        .isBadRequest())
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
        String response = result.getResponse().getContentAsString();
        Assert.isTrue(response.contains("Sample Error"), "Test Case Failure:: Invalid Error Match");
    }

    /**
     * Negative test case with updateWidget method throwing InvalidParameterValueException
     * Expected 400 Bad Request
     */
    @Test
    public void testUpdateWidgetWithExceptionFromWidgetService() throws Exception {
        long id = 1;
        Mockito.when(widgetService.updateWidget(any(), any())).thenThrow(new Exception("Sample Error"));
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = ow.writeValueAsString(updateDto);
        MockHttpServletRequestBuilder builder = ControllerTestUtility.patchBuilder("/api/v1/widgets/" + id , json);
        MvcResult result = this.mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status()
                        .isInternalServerError())
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
        String response = result.getResponse().getContentAsString();
        Assert.isTrue(response.contains("Sample Error"), "Test Case Failure:: Invalid Error Match");
    }
}
