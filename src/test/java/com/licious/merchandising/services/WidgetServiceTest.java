package com.licious.merchandising.services;

import com.licious.merchandising.dtos.mappers.*;
import com.licious.merchandising.dtos.models.WidgetMappingDto;
import com.licious.merchandising.dtos.request.WidgetCreationRequestV1Dto;
import com.licious.merchandising.dtos.response.WidgetCreationResponseV1Dto;
import com.licious.merchandising.enumerations.WidgetType;
import com.licious.merchandising.exceptions.InvalidParameterValueException;
import com.licious.merchandising.models.Widget;
import com.licious.merchandising.models.WidgetMapping;
import com.licious.merchandising.repositories.WidgetRepository;
import com.licious.merchandising.services.impl.WidgetServiceImpl;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mapstruct.factory.Mappers;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import static com.licious.merchandising.enumerations.ProductWidgetMappingType.RULE_BASED;
import static com.licious.merchandising.utils.ErrorMessages.CATEGORY_ID_NOT_NULL;
import static com.licious.merchandising.utils.ErrorMessages.RULE_ID_NULL;
import static com.licious.merchandising.utils.ErrorMessages.WIDGET_ID_MISMATCH;
import static com.licious.merchandising.utils.ErrorMessages.WIDGET_ID_NULL;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.any;

@SpringBootTest(classes = {WidgetMappingDto.class, WidgetMapper.class, WidgetCreationResponseV1Dto.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class WidgetServiceTest {
    private WidgetService widgetService;
    private final CollectionWidgetMapper collectionWidgetMapper = Mappers.getMapper(CollectionWidgetMapper.class);
    private final PageWidgetMapper pageWidgetMapper = Mappers.getMapper(PageWidgetMapper.class);
    private final ProductWidgetMapper productWidgetMapper = Mappers.getMapper(ProductWidgetMapper.class);
    private final WidgetMappingMapper widgetMappingMapper = Mappers.getMapper(WidgetMappingMapper.class);
    private final WidgetMapper widgetMapper = Mappers.getMapper(WidgetMapper.class);
    private WidgetRepository widgetRepository;
    private WidgetCreationRequestV1Dto updateDto;
    private Widget widget;
    private Widget updatedWidget;

    @BeforeAll
    void setup() throws IllegalAccessException {
        FieldUtils.writeField(widgetMapper, "widgetMappingMapper", widgetMappingMapper, true);
        this.widgetRepository = Mockito.mock(WidgetRepository.class);
        this.widgetService = new WidgetServiceImpl(widgetRepository, collectionWidgetMapper, pageWidgetMapper, productWidgetMapper, widgetMapper);
        updateDto = new WidgetCreationRequestV1Dto();
        WidgetMappingDto mappingDto = new WidgetMappingDto();
        mappingDto.setId(1L);
        mappingDto.setAppImage("testImageURL");
        mappingDto.setAppUrl("testUrl");
        mappingDto.setWebImage("testImageURL");
        mappingDto.setWebUrl("testUrl");
        mappingDto.setMsiteImage("testImageURL");
        mappingDto.setCategoryId(1L);
        mappingDto.setCardName("cardName");
        mappingDto.setCardPosition(1);
        List<WidgetMappingDto> mappings = new ArrayList<>();
        mappings.add(mappingDto);

        updateDto.setId(1L);
        updateDto.setAndroidEnabled(true);
        updateDto.setIosEnabled(true);
        updateDto.setWebEnabled(true);
        updateDto.setMsiteEnabled(true);
        updateDto.setLayoutName("short1");
        updateDto.setLayoutAspectRatio("160x180");
        updateDto.setName("updatedName");
        updateDto.setType("COLLECTION");
        updateDto.setSubText("testSubText");
        updateDto.setStatus("ACTIVE");
        updateDto.setWidgetMappings(mappings);

        WidgetMapping widgetMapping = new WidgetMapping();
        widgetMapping.setId(1L);
        widgetMapping.setCardName("testCardName");
        widgetMapping.setCardPosition(1);
        widgetMapping.setAppImage("testImage");
        widgetMapping.setWebImage("testImage");
        widgetMapping.setMsiteImage("testImage");
        widgetMapping.setAppUrl("testAppUrl");
        widgetMapping.setWebUrl("testWebUrl");
        widgetMapping.setCategoryId(1L);

        Set<WidgetMapping> widgetMappings = new HashSet<>();
        widgetMappings.add(widgetMapping);
        widget = new Widget();
        widget.setId(1L);
        widget.setName("widgetName");
        widget.setAndroidEnabled(true);
        widget.setIosEnabled(true);
        widget.setMsiteEnabled(true);
        widget.setWebEnabled(true);
        widget.setLayoutName("short1");
        widget.setLayoutAspectRatio("160x180");
        widget.setStatus("ACTIVE");
        widget.setSubText("testSubText");
        widget.setWidgetMappings(widgetMappings);
        widgetMapping.setWidget(widget);

        updatedWidget = widget;
        updatedWidget.setName("updatedName");

    }

    /**
     * Update Widget with valid payload
     * @throws Exception
     */
    @Test
    public void testUpdateWidget() throws Exception {
        when(widgetRepository.findById(anyLong())).thenReturn(Optional.ofNullable(widget));
        when(widgetRepository.save(any())).thenReturn(updatedWidget);
        WidgetCreationResponseV1Dto updateResponse = widgetService.updateWidget(updateDto, updateDto.getId());
        Assert.isTrue(updateDto.getName().equals(updateResponse.getName()), "Assertion Success");
    }

    /**
     * Update widget with widget id null in body
     */
    @Test
    void testUpdateWidgetWithInvalidRequest() {
        final var request = WidgetCreationRequestV1Dto.builder()
                .name("testName").widgetMappings(updateDto.getWidgetMappings()).build();

        Exception exception = assertThrows(InvalidParameterValueException.class, () -> {
            widgetService.updateWidget(request, 1L);
        });

        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(WIDGET_ID_NULL));
    }

    /**
     * Update widget with widgetId is URL and body mismatch
     */
    @Test
    void testUpdateWidgetWithInvalidRequestWithIdMismatch() {
        final var request = WidgetCreationRequestV1Dto
                .builder().name("testName").id(2L).widgetMappings(updateDto.getWidgetMappings()).build();

        Exception exception = assertThrows(InvalidParameterValueException.class, () -> {
            widgetService.updateWidget(request, 1L);
        });

        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(WIDGET_ID_MISMATCH));
    }

    /**
     * update widget of RULE_BASED type with categoryId not null and ruleId null
     */
    @Test
    void testUpdateWidgetWithInvalidRequestWithInvalidRuleId() {
        WidgetCreationRequestV1Dto ruleIdUpdateDto = updateDto;
        ruleIdUpdateDto.setType(WidgetType.PRODUCT.getValue());
        ruleIdUpdateDto.setMappingType(RULE_BASED.getValue());

        Exception exception = assertThrows(InvalidParameterValueException.class, () -> {
            widgetService.updateWidget(ruleIdUpdateDto, 1L);
        });
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(CATEGORY_ID_NOT_NULL));
    }
}
