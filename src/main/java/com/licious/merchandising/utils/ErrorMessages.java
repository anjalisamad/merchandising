package com.licious.merchandising.utils;

import lombok.experimental.UtilityClass;

@UtilityClass
public class ErrorMessages {
    public static final String WIDGET_NOT_FOUND = "Widget not found for id:: %s";
    public static final String INVALID_WIDGET_TYPE = "Invalid Widget Type:: %s";
    public static final String WIDGET_ID_NOT_NULL = "Widget Id is Not Null, please provide valid value to create the " +
            "data";
    public static final String TYPE_NULL = "Type is Null, please provide valid type of widget to create the data";
    public static final String CATEGORY_ID_NULL = "Category Id is Null, please provide valid data";
    public static final String CATEGORY_ID_NOT_NULL = "Category Id is not Null, please provide valid data as per the widget type";
    public static final String PRODUCT_COLLECTION_ID_NULL = "Product Collection Id is Null, please provide valid data for widget creation";
    public static final String RULE_ID_NULL = "Rule Id is Null, please provide valid data for widget creation";
    public static final String APP_URL_NULL = "App Url is Null, please provide valid data for widget creation";
    public static final String WEB_URL_NULL = "Web Url is Null, please provide valid data for widget creation";
    public static final String APP_IMAGE_NULL = "App Image is Null, please provide valid data for widget creation";
    public static final String WEB_IMAGE_NULL = "Web Image is Null, please provide valid data for widget creation";
    public static final String MSITE_IMAGE_NULL = "Msite Image is Null, please provide valid data for widget creation";
    public static final String CARD_NAME_NULL = "Card Name is Null, please provide valid data for widget creation";
    public static final String CARD_POSITION_NULL = "Card Position is Null, please provide valid data for widget " +
            "creation";
    public static final String APP_URL_NOT_NULL = "App Url is not Null, please provide valid data as per the widget type";
    public static final String WEB_URL_NOT_NULL = "Web Url is not Null, please provide valid data as per the widget type";
    public static final String APP_IMAGE_NOT_NULL = "App Image is not Null, please provide valid data as per the " +
            "widget type";
    public static final String WEB_IMAGE_NOT_NULL = "Web Image is not Null, please provide valid data as per the widget type";
    public static final String MSITE_IMAGE_NOT_NULL = "Msite Image is not Null, please provide valid data as per the " +
            "widget type";
    public static final String CARD_NAME_NOT_NULL = "Card Name is not Null, please provide valid data as per the " +
            "widget type";
    public static final String CARD_POSITION_NOT_NULL = "Card Position is not Null, please provide valid data as per " +
            "the widget type";
    public static final String PRODUCT_COLLECTION_ID_NOT_NULL = "Product Collection Id is not Null, please provide valid data as per the widget type";
    public static final String RULE_ID_NOT_NULL = "Rule Id is not Null, please provide valid data as per the widget type";
    public static final String MAPPING_TYPE_NOT_NULL = "Mapping Type is not Null, please provide valid data " +
            "as per the widget type";
    public static final String RULE_ID_MAPPING_TYPE_MISMATCH = "Rule Id should be Null for mapping type Manual, " +
            "please provide valid data as per the widget type";
    public static final String PRODUCT_COLLECTION_ID_MAPPING_TYPE_MISMATCH = "Product Collection Id should be Null " +
            "for mapping type Rule Based, please provide valid data as per the widget type";
    public static final String PRODUCT_WIDGET_MAPPING_MISMATCH = "Widget Mapping size shouldn't be greater than 1 " +
            "for Product widget type, please provide valid data as per the widget type";

    public static final String WIDGET_ID_NULL = "Widget Id is empty, please provide valid value to update the data";
    public static final String WIDGET_ID_MISMATCH = "Widget id in request and path do not match";

    public static final String VARIANT_USER_MAPPING_NOT_FOUND = "No active variant mapping found for cityId: %s and user segment: %s";
    public static final String VARIANT_WIDGET_MAPPING_NOT_FOUND = "No variant widget mapping found for cityId: %s and user segment: %s and variantId: %s";
    public static final String INVALID_CITY_ID_AND_SEGMENT_VALUES = "City Id or Segment information not present. Will return default response";
    public static final String VARIANT_NOT_FOUND = "No home page variant for for variantId: %s";
    public static final String WIDGET_MAPPINGS_FOR_VARIANT_NOT_FOUND = "No variant widget mapping found for variantId: %s";

    public static final String INVALID_WIDGET_MAPPINGS = "Invalid number of widget mappings for widget id: %s";

}
