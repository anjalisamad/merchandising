package com.licious.merchandising.utils;

import com.licious.merchandising.dtos.exceptions.ErrorMetaDto;
import com.licious.merchandising.dtos.exceptions.ErrorResponseDto;
import com.licious.merchandising.dtos.response.GenericResponseDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;

import java.io.Serializable;
import java.util.List;

@Service
public class ResponseResolver<T> implements Serializable {

    public ResponseEntity<GenericResponseDto<?>> responseResolver(Integer code, T responseData) {
        GenericResponseDto<?> response;
        HttpStatus httpStatus;

        if (code == HttpStatus.OK.value()) {
            response = new GenericResponseDto<>(responseData);
            httpStatus = HttpStatus.OK;
        } else if (code == HttpStatus.CREATED.value()) {
            response = new GenericResponseDto<>(responseData);
            httpStatus = HttpStatus.CREATED;
        } else {
            response = new GenericResponseDto<>(null);
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        }

        return new ResponseEntity<>(response, httpStatus);
    }

    public ResponseEntity<GenericResponseDto<?>> responseResolver(Integer code, T responseData, MultiValueMap<String,String> headers) {
        GenericResponseDto<?> response;
        HttpStatus httpStatus;

        if (code == HttpStatus.OK.value()) {
            response = new GenericResponseDto<>(responseData);
            httpStatus = HttpStatus.OK;
        } else if (code == HttpStatus.CREATED.value()) {
            response = new GenericResponseDto<>(responseData);
            httpStatus = HttpStatus.CREATED;
        } else {
            response = new GenericResponseDto<>(null);
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        }

        return new ResponseEntity<>(response, headers ,httpStatus);
    }

    public ResponseEntity<ErrorResponseDto> resolveErrorResponse(Integer code, List<ErrorMetaDto> errors){
        ErrorResponseDto response;
        HttpStatus httpStatus;

        if (code == HttpStatus.BAD_REQUEST.value()) {
            response = new ErrorResponseDto(errors);
            httpStatus = HttpStatus.BAD_REQUEST;
        } else if (code == HttpStatus.UNAUTHORIZED.value()) {
            response = new ErrorResponseDto(errors);
            httpStatus = HttpStatus.UNAUTHORIZED;
        } else if (code == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
            response = new ErrorResponseDto(errors);
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        } else if (code == HttpStatus.NOT_FOUND.value()) {
            response = new ErrorResponseDto(errors);
            httpStatus = HttpStatus.NOT_FOUND;
        }
        else {
            response = new ErrorResponseDto(errors);
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        }

        return new ResponseEntity<>(response ,httpStatus);
    }

    public ResponseEntity<ErrorResponseDto> resolveErrorResponse(Integer code, List<ErrorMetaDto> errors, MultiValueMap<String,String> header){
        ErrorResponseDto response;
        HttpStatus httpStatus;

        if (code == HttpStatus.BAD_REQUEST.value()) {
            response = new ErrorResponseDto(errors);
            httpStatus = HttpStatus.BAD_REQUEST;
        } else if (code == HttpStatus.UNAUTHORIZED.value()) {
            response = new ErrorResponseDto(errors);
            httpStatus = HttpStatus.UNAUTHORIZED;
        } else if (code == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
            response = new ErrorResponseDto(errors);
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        } else if (code == HttpStatus.NOT_FOUND.value()) {
            response = new ErrorResponseDto(errors);
            httpStatus = HttpStatus.NOT_FOUND;
        }
        else {
            response = new ErrorResponseDto(errors);
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        }

        return new ResponseEntity<>(response, header, httpStatus);
    }

}

