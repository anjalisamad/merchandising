package com.licious.merchandising.utils;

import lombok.experimental.UtilityClass;

import java.time.Duration;

@UtilityClass
public class Constants {

    @UtilityClass
    public static class CommonFormats {
        public static final String RESPONSE_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";
        public static final String DB_STRING_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'+'";
    }

    @UtilityClass
    public static class PathVariables {
        public static final String WIDGET_ID = "/{widget_id}";
    }

    @UtilityClass
    public static class UrlPathParameters {
        public static final String WIDGET_ID = "widget_id";
        public static final String VARIANT_ID = "variant_id";
        public static final String CITY_ID = "city_id";
        public static final String SEGMENT = "segment";

    }

    @UtilityClass
    public static class Routes {
        public static final String HOMEPAGE_BASE = "/api/v1/homepage";
        public static final String WIDGET_BASE = "/api/v1/widgets";
        public static final String HOMEPAGE_BY_VARIANT_ID ="/preview/{variant_id}";
    }

    public static class CacheConstants {
        public static final String HOMEPAGE_CACHE_KEY = "HOMEPAGE_CITY_ID_AND_SEGMENT";
        public static final Duration CACHE_TTL_5_MINUTES = Duration.ofMinutes(5);
    }
}
