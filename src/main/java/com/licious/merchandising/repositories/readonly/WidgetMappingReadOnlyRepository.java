package com.licious.merchandising.repositories.readonly;

import com.licious.merchandising.annotations.ReadOnlyRepository;
import com.licious.merchandising.models.WidgetMapping;
import org.springframework.data.jpa.repository.JpaRepository;

@ReadOnlyRepository
public interface WidgetMappingReadOnlyRepository extends JpaRepository<WidgetMapping, Long> {
}
