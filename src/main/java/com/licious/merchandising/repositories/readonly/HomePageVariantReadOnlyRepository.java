package com.licious.merchandising.repositories.readonly;

import com.licious.merchandising.annotations.ReadOnlyRepository;
import com.licious.merchandising.models.HomePageVariant;
import org.springframework.data.jpa.repository.JpaRepository;

@ReadOnlyRepository
public interface HomePageVariantReadOnlyRepository extends JpaRepository<HomePageVariant, Long> {
}
