package com.licious.merchandising.repositories.readonly;

import com.licious.merchandising.annotations.ReadOnlyRepository;
import com.licious.merchandising.models.Widget;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

@ReadOnlyRepository
public interface WidgetReadOnlyRepository extends JpaRepository<Widget, Long> {
    List<Widget> findOneByType(String type);
}
