package com.licious.merchandising.repositories.readonly;

import com.licious.merchandising.annotations.ReadOnlyRepository;
import com.licious.merchandising.models.HomePageVariantWidgetMapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.lang.NonNull;

import java.util.List;
import java.util.Optional;

@ReadOnlyRepository
public interface HomePageVariantWidgetMappingReadOnlyRepository extends JpaRepository<HomePageVariantWidgetMapping, Long> {
    String VARIANT_WIDGET_MAPPING_BY_VARIANT_ID = """
            SELECT 
             hpvwm
            FROM
            HomePageVariantWidgetMapping hpvwm
            WHERE variantId = :variantId
            """;

    @Query(VARIANT_WIDGET_MAPPING_BY_VARIANT_ID)
    List<HomePageVariantWidgetMapping> findByVariantId(
            @NonNull @Param("variantId") Long variantId
    );
}
