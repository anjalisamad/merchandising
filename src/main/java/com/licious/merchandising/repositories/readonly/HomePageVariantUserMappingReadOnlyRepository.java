package com.licious.merchandising.repositories.readonly;

import com.licious.merchandising.annotations.ReadOnlyRepository;
import com.licious.merchandising.models.HomePageVariantUserMapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.lang.NonNull;

import java.util.List;
import java.util.Optional;

@ReadOnlyRepository
public interface HomePageVariantUserMappingReadOnlyRepository extends JpaRepository<HomePageVariantUserMapping, Long> {
    String VARIANT_USER_MAPPING_BY_CITY_AND_SEGMENT = """
            SELECT vum
            FROM HomePageVariantUserMapping vum
            INNER JOIN HomePageVariant hpv
            ON vum.homePageVariant.id = hpv.id
            WHERE hpv.status = 1
            AND vum.segment = :segment
            AND vum.city = :cityId
            """;

    @Query(VARIANT_USER_MAPPING_BY_CITY_AND_SEGMENT)
    Optional<HomePageVariantUserMapping> findMappingsByCityIdAndSegment(
        @NonNull @Param("cityId") Long cityId,
        @NonNull @Param("segment") String userSegment
        );
}
