package com.licious.merchandising.repositories;

import com.licious.merchandising.models.WidgetMapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WidgetMappingRepository extends JpaRepository<WidgetMapping, Long> {
}
