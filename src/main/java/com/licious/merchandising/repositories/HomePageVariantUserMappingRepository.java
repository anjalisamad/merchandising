package com.licious.merchandising.repositories;

import com.licious.merchandising.models.HomePageVariantUserMapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HomePageVariantUserMappingRepository extends JpaRepository<HomePageVariantUserMapping, Long> {
}
