package com.licious.merchandising.repositories;

import com.licious.merchandising.models.HomePageVariantWidgetMapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HomePageVariantWidgetMappingRepository extends JpaRepository<HomePageVariantWidgetMapping, Long> {
}
