package com.licious.merchandising.repositories;

import com.licious.merchandising.models.HomePageVariant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HomePageVariantRepository extends JpaRepository<HomePageVariant, Long> {
}
