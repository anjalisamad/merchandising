package com.licious.merchandising.exceptions;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;

public class ResourceNotFoundException extends MerchandisingException {
    private final HttpStatus httpStatusCode;

    public ResourceNotFoundException(String message) {
        this(message, 400, HttpStatus.BAD_REQUEST);
    }

    public ResourceNotFoundException(String message, int errorCode, HttpStatus httpStatusCode) {
        super(message, errorCode);
        this.httpStatusCode = httpStatusCode;
    }
}
