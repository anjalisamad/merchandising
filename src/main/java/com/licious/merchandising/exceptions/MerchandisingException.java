package com.licious.merchandising.exceptions;

import lombok.Getter;
import org.springframework.lang.NonNull;

public class MerchandisingException extends RuntimeException{
    @Getter
    private final int errorCode;

    public MerchandisingException(@NonNull String message) {
        super(message);
        this.errorCode = 400;
    }

    public MerchandisingException(@NonNull String message, int errorCode){
        super(message);
        this.errorCode = errorCode;
    }
}
