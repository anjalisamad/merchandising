package com.licious.merchandising.exceptions;

import org.springframework.lang.NonNull;

public class InvalidParameterValueException extends MerchandisingException{

    public InvalidParameterValueException(@NonNull String message) {
        super(message, 400);
    }
}
