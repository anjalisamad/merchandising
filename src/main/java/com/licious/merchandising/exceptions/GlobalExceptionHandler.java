package com.licious.merchandising.exceptions;

import com.licious.merchandising.dtos.exceptions.ErrorMetaDto;
import com.licious.merchandising.dtos.exceptions.ErrorResponseDto;
import com.licious.merchandising.dtos.response.WidgetCreationResponseV1Dto;
import com.licious.merchandising.utils.ResponseResolver;
import org.springframework.core.NestedExceptionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.List;

@ControllerAdvice
public class GlobalExceptionHandler {


    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<ErrorResponseDto> messageNotReadableException(HttpMessageNotReadableException exception){
        ResponseResolver<WidgetCreationResponseV1Dto> responseResolver = new ResponseResolver<>();
        final var message = NestedExceptionUtils.getMostSpecificCause(exception).getMessage();
        final var exceptionDto = new ErrorMetaDto(String.valueOf(HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST.toString(), message);
        return responseResolver.resolveErrorResponse(HttpStatus.BAD_REQUEST.value(), List.of(exceptionDto));
    }

    @ExceptionHandler(InvalidParameterValueException.class)
    public ResponseEntity<ErrorResponseDto> invalidParameterException(InvalidParameterValueException exception){
        ResponseResolver<WidgetCreationResponseV1Dto> responseResolver = new ResponseResolver<>();
        final var message = NestedExceptionUtils.getMostSpecificCause(exception).getMessage();
        final var exceptionDto = new ErrorMetaDto(String.valueOf(HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST.toString(), message);
        return responseResolver.resolveErrorResponse(HttpStatus.BAD_REQUEST.value(), List.of(exceptionDto));
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorResponseDto> methodArgumentNotValidException(MethodArgumentNotValidException exception){
        ResponseResolver<WidgetCreationResponseV1Dto> responseResolver = new ResponseResolver<>();
        final var message = NestedExceptionUtils.getMostSpecificCause(exception).getMessage();
        final var exceptionDto = new ErrorMetaDto(String.valueOf(HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST.toString(), message);
        return responseResolver.resolveErrorResponse(HttpStatus.BAD_REQUEST.value(), List.of(exceptionDto));
    }
}


