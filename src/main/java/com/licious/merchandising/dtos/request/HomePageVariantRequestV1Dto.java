package com.licious.merchandising.dtos.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.licious.merchandising.dtos.models.HomePageVariantUserMappingDto;
import com.licious.merchandising.dtos.models.HomePageVariantWidgetsDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class HomePageVariantRequestV1Dto {

    private Long id;

    @Valid @NotEmpty(message = "'name' can't be empty")
    private String name;

    @Valid @NotEmpty(message = "'status' can't be empty")
    private String status;

//    @Valid @NotEmpty(message = "'segment' can't be empty")
//    private String segment;
//
//    @Valid @NotEmpty(message = "'city' can't be empty")
//    private String city;

    @Valid @NotNull(message = "'homePageVariantUserMappings' can't be Null")
    private List<HomePageVariantUserMappingDto> users;

    @Valid @NotNull(message = "'homePageVariantWidgetMappings' can't be Null")
    private List<HomePageVariantWidgetsDto> widgets;
}
