package com.licious.merchandising.dtos.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.licious.merchandising.dtos.models.WidgetMappingDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WidgetCreationRequestV1Dto {
    private Long id;

    @Valid @NotEmpty(message = "'name' can't be empty")
    private String name;

    @Valid @NotEmpty(message = "'type' can't be empty")
    private String type;
    @Valid
    private String subText;

    @NotNull
    private Boolean iosEnabled;

    @NotNull
    private Boolean androidEnabled;

    @NotNull
    private Boolean webEnabled;

    @NotNull
    private Boolean msiteEnabled;

    @Valid @NotEmpty(message = "'layoutName' can't be empty")
    private String layoutName;

    @Valid @NotEmpty(message = "'layoutAspectRatio' can't be empty")
    private String layoutAspectRatio;

    @Valid @NotEmpty(message = "'status' can't be empty")
    private String status;

    @Valid
    private String mappingType;

    @Valid @NotNull(message = "'widgetMappings' can't be Null")
    private List<WidgetMappingDto> widgetMappings;
}
