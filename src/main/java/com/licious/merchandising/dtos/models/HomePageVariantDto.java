package com.licious.merchandising.dtos.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.licious.merchandising.utils.Constants;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Calendar;
import java.util.Set;

@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Builder
@Getter
@Setter
public class HomePageVariantDto {
    private Long id;
	private String name;
	private Boolean status;
	private Set<HomePageVariantUserMappingDto> homePageVariantUserMappings;
	private Set<HomePageVariantWidgetMappingDto> homePageVariantWidgetMappings;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.CommonFormats.RESPONSE_DATE_FORMAT)
	private Calendar createdAt;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.CommonFormats.RESPONSE_DATE_FORMAT)
	private Calendar updatedAt;
}
