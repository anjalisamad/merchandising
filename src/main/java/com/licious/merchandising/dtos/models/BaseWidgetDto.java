package com.licious.merchandising.dtos.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.licious.merchandising.utils.Constants;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.util.Calendar;
import java.util.Set;

@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class BaseWidgetDto {
    private Long id;
	private String name;
	private String type;
	private String subText;
	private Boolean iosEnabled;
	private Boolean androidEnabled;
	private Boolean webEnabled;
	private Boolean msiteEnabled;
	private String layoutName;
	private String layoutAspectRatio;
	private Boolean status;
	private Set<HomePageVariantWidgetMappingDto> homePageVariantWidgetMappings;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.CommonFormats.RESPONSE_DATE_FORMAT)
	@CreationTimestamp
	private Calendar createdAt;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.CommonFormats.RESPONSE_DATE_FORMAT)
	@UpdateTimestamp
	private Calendar updatedAt;
}
