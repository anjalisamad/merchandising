package com.licious.merchandising.dtos.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Calendar;

@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Builder
@Getter
@Setter
public class HomePageVariantWidgetsDto {
    private Long id;
    private Long variantId;
    private Long widgetId;
    private String name;
    private Integer order;
    private String status;

    private Calendar createdAt;
    private Calendar updatedAt;
}
