package com.licious.merchandising.dtos.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.AccessLevel;
import java.util.Calendar;

@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor
@Builder
@Getter
@Setter
public class WidgetMappingDto {
    private Long id;
    private String cardName;
    private Integer cardPosition;
    private String webImage;
    private String msiteImage;
    private String appImage;
    private String webUrl;
    private String appUrl;
    private Long productCollectionId;
    private Long ruleId;
    private Long categoryId;

    private Calendar createdAt;
    private Calendar updatedAt;
}
