package com.licious.merchandising.dtos.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.licious.merchandising.utils.Constants;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.util.Calendar;

@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class BaseWidgetMappingDto {
	private Long id;
	private String cardName;
	private Integer cardPosition;
	private String webImage;
	private String msiteImage;
	private String appImage;
	private String webUrl;
	private String appUrl;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.CommonFormats.RESPONSE_DATE_FORMAT)
	@CreationTimestamp
	private Calendar createdAt;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.CommonFormats.RESPONSE_DATE_FORMAT)
	@UpdateTimestamp
	private Calendar updatedAt;
}
