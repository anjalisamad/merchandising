package com.licious.merchandising.dtos.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.licious.merchandising.dtos.models.WidgetMappingDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Calendar;
import java.util.List;

@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WidgetCreationResponseV1Dto {
    private Long id;
    private String name;
    private String type;
    private String subText;
    private Boolean iosEnabled;
    private Boolean androidEnabled;
    private Boolean webEnabled;
    private Boolean msiteEnabled;
    private String layoutName;
    private String layoutAspectRatio;
    private String status;
    private String mappingType;
    private Calendar createdAt;
    private Calendar updatedAt;
    private List<WidgetMappingDto> widgetMappings;
}
