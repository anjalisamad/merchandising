package com.licious.merchandising.dtos.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.List;

@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class HomePageResponseDto {
    private Long id;
    private String name;
    private String status;
    private String segment;
    private Long city;
    private List<HomepageWidgetsDto> widgets;
}
