package com.licious.merchandising.dtos.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.licious.merchandising.dtos.models.WidgetMappingDto;
import lombok.*;

import java.util.List;

@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class HomepageWidgetsDto {
    private Long id;
    private Long order;
    private String name;
    private String type;
    private String subText;
    private Boolean iosEnabled;
    private Boolean androidEnabled;
    private Boolean webEnabled;
    private Boolean msiteEnabled;
    private String layoutName;
    private String layoutAspectRatio;
    private String status;
    private String mappingType;
    private Long productCollectionId;
    private Long ruleId;
    private List<WidgetMappingDto> widgetMappings;
}
