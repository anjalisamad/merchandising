package com.licious.merchandising.dtos.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class GenericResponseDto<T> {
    private T data;
}
