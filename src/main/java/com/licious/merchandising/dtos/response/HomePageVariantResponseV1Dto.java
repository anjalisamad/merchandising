package com.licious.merchandising.dtos.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.licious.merchandising.dtos.models.HomePageVariantUserMappingDto;
import com.licious.merchandising.dtos.models.HomePageVariantWidgetsDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Calendar;
import java.util.List;

@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class HomePageVariantResponseV1Dto {
    private Long id;
    private String name;
    private String status;
//    private String segment;
//    private String city;
    private Calendar createdAt;
    private Calendar updatedAt;
    private List<HomePageVariantUserMappingDto> users;
    private List<HomePageVariantWidgetsDto> widgets;
}
