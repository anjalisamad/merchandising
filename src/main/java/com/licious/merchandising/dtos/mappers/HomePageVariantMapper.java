package com.licious.merchandising.dtos.mappers;

import com.licious.merchandising.dtos.models.HomePageVariantDto;
import com.licious.merchandising.dtos.request.HomePageVariantRequestV1Dto;
import com.licious.merchandising.dtos.response.HomePageVariantResponseV1Dto;
import com.licious.merchandising.models.HomePageVariant;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = {HomePageVariantWidgetsMapper.class, HomePageVariantUserMappingMapper.class})
public interface HomePageVariantMapper {
    HomePageVariantDto toHomePageVariantDto(HomePageVariant homePageVariant);
    HomePageVariant toHomePageVariant(HomePageVariantDto homePageVariantDto);
    List<HomePageVariantDto> toHomePageVariantDtos(List<HomePageVariant> homePageVariants);
    List<HomePageVariant> toHomePageVariants(List<HomePageVariantDto> homePageVariantDtos);

    @Mapping(source = "homePageVariantRequestV1Dto.widgets", target = "homePageVariantWidgetMappings")
    @Mapping(source = "homePageVariantRequestV1Dto.users", target = "homePageVariantUserMappings")
    HomePageVariant convertRequestDtoToEntity(HomePageVariantRequestV1Dto homePageVariantRequestV1Dto);
    List<HomePageVariant> convertRequestDtoToEntitys(List<HomePageVariantRequestV1Dto> homePageVariantRequestV1Dtos);

    @Mapping(source = "homePageVariant.homePageVariantWidgetMappings", target = "widgets")
    @Mapping(source = "homePageVariant.homePageVariantUserMappings", target = "users")
    HomePageVariantResponseV1Dto convertEntityToResponseDto(HomePageVariant homePageVariant);
    List<HomePageVariantResponseV1Dto> convertEntityToResponseDtos(List<HomePageVariant> homePageVariants);
}
