package com.licious.merchandising.dtos.mappers;

import com.licious.merchandising.dtos.models.ProductWidgetMappingDto;
import com.licious.merchandising.models.WidgetMapping;

import java.util.List;

public interface ProductWidgetMappingMapper {
    ProductWidgetMappingDto toProductWidgetMappingDto(WidgetMapping widgetMapping);
    List<ProductWidgetMappingDto> toProductWidgetMappingDtos(List<WidgetMapping> widgetMappings);
    WidgetMapping toWidgetMapping(ProductWidgetMappingDto productWidgetMappingDto);
    List<WidgetMapping> toWidgetMappings(List<ProductWidgetMappingDto> productWidgetMappingDtos);
}
