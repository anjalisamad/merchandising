package com.licious.merchandising.dtos.mappers;

import com.licious.merchandising.dtos.models.WidgetMappingDto;
import com.licious.merchandising.models.WidgetMapping;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = {ProductWidgetMappingMapper.class, PageWidgetMappingMapper.class})
public interface WidgetMappingMapper {
    WidgetMappingDto toWidgetMappingDto(WidgetMapping widgetMapping);
    List<WidgetMappingDto> toWidgetMappingDtos(List<WidgetMapping> widgetMappings);
    WidgetMapping toWidgetMapping(WidgetMappingDto widgetMappingDto);
    List<WidgetMapping> toWidgetMappings(List<WidgetMappingDto> widgetMappingDtos);
}
