package com.licious.merchandising.dtos.mappers;

import com.licious.merchandising.dtos.models.CollectionWidgetMappingDto;
import com.licious.merchandising.models.WidgetMapping;
import org.mapstruct.Mapper;

import java.util.List;
@Mapper(componentModel = "spring")
public interface CollectionWidgetMappingMapper {
    CollectionWidgetMappingDto toCollectionWidgetMappingDto(WidgetMapping widgetMapping);
    List<CollectionWidgetMappingDto> toCollectionWidgetMappingDtos(List<WidgetMapping> widgetMappingList);
    List<WidgetMapping> towidgetMappings(List<CollectionWidgetMappingDto> toCollectionWidgetMappingDtos);
    WidgetMapping toWidgetMapping(CollectionWidgetMappingDto toCollectionWidgetMappingDto);
}
