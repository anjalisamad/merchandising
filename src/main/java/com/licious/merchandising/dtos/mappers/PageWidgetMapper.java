package com.licious.merchandising.dtos.mappers;

import com.licious.merchandising.dtos.models.PageWidgetDto;
import com.licious.merchandising.dtos.request.WidgetCreationRequestV1Dto;
import com.licious.merchandising.dtos.response.WidgetCreationResponseV1Dto;
import com.licious.merchandising.models.Widget;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = PageWidgetMappingMapper.class)
public interface PageWidgetMapper {
    PageWidgetDto toPageWidgetDto(Widget widget);
    List<PageWidgetDto> toWidgetDtos(List<Widget> widgets);
    List<Widget> toWidgets(List<PageWidgetDto> toWidgetDtos);
    Widget toWidget(PageWidgetDto widgetDto);


    Widget convertRequestDtoToEntity(WidgetCreationRequestV1Dto widgetCreationRequestV1Dto);
    List<Widget> convertRequestDtoToEntitys(List<WidgetCreationRequestV1Dto> widgetCreationRequestV1Dtos);
    WidgetCreationResponseV1Dto convertEntityToResponseDto(Widget widget);
    List<WidgetCreationResponseV1Dto> convertEntityToResponseDtos(List<Widget> widgets);
}
