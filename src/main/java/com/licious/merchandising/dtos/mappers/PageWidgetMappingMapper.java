package com.licious.merchandising.dtos.mappers;

import com.licious.merchandising.dtos.models.PageWidgetMappingDto;
import com.licious.merchandising.models.WidgetMapping;
import org.mapstruct.Mapper;

import java.util.List;
@Mapper(componentModel = "spring")
public interface PageWidgetMappingMapper {
    PageWidgetMappingDto toPageWidgetMappingDto(WidgetMapping widgetMapping);
    List<PageWidgetMappingDto> toPageWidgetMappingDtos(List<WidgetMapping> widgetMappings);
    WidgetMapping toWidgetMapping(PageWidgetMappingDto pageWidgetMappingDto);
    List<WidgetMapping> toWidgetMappings(List<PageWidgetMappingDto> pageWidgetMappingDtos);
}

