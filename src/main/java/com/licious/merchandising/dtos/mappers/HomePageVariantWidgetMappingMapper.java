package com.licious.merchandising.dtos.mappers;

import com.licious.merchandising.dtos.models.HomePageVariantWidgetMappingDto;
import com.licious.merchandising.models.HomePageVariantWidgetMapping;
import org.mapstruct.Mapper;

import java.util.List;


@Mapper(componentModel = "spring")
public interface HomePageVariantWidgetMappingMapper {
    HomePageVariantWidgetMappingDto toHomePageVariantWidgetMappingDto(HomePageVariantWidgetMapping homePageVariantWidgetMapping);
    List<HomePageVariantWidgetMappingDto> toHomePageVariantWidgetMappingDtos(List<HomePageVariantWidgetMapping> homePageVariantWidgetMappingList);
    List<HomePageVariantWidgetMapping> toHomePageVariantWidgetMappings(List<HomePageVariantWidgetMappingDto> toHpVariantWidgetMappingDtos);
    HomePageVariantWidgetMapping toHomePageVariantWidgetMapping(HomePageVariantWidgetMappingDto toHpVariantWidgetMappingDto);


}
