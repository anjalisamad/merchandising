package com.licious.merchandising.dtos.mappers;

import com.licious.merchandising.dtos.models.HomePageVariantWidgetsDto;
import com.licious.merchandising.models.HomePageVariantWidgetMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface HomePageVariantWidgetsMapper {
    @Mapping(source = "homePageVariantWidgetMapping.position", target = "order")
    HomePageVariantWidgetsDto toHomePageVariantWidgetMappingDto(HomePageVariantWidgetMapping homePageVariantWidgetMapping);
    List<HomePageVariantWidgetsDto> toHomePageVariantWidgetMappingDtos(List<HomePageVariantWidgetMapping> homePageVariantWidgetMappings);
    @Mapping(source = "homePageVariantWidgetsDto.order", target = "position")
    HomePageVariantWidgetMapping toHomePageVariantWidgetMapping(HomePageVariantWidgetsDto homePageVariantWidgetsDto);
    List<HomePageVariantWidgetMapping> toHomePageVariantWidgetMappings(List<HomePageVariantWidgetsDto> homePageVariantWidgetsDtos);
}
