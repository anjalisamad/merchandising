package com.licious.merchandising.dtos.mappers;

import com.licious.merchandising.dtos.models.CollectionWidgetDto;
import com.licious.merchandising.dtos.request.WidgetCreationRequestV1Dto;
import com.licious.merchandising.dtos.response.WidgetCreationResponseV1Dto;
import com.licious.merchandising.models.Widget;
import org.mapstruct.Mapper;

import java.util.List;
@Mapper(componentModel = "spring", uses = CollectionWidgetMappingMapper.class)
public interface CollectionWidgetMapper {
	CollectionWidgetDto toCollectionWidgetDto(Widget widget);
	List<CollectionWidgetDto> toCollectionWidgetDtos(List<Widget> widgets);
	List<Widget> listWidget (List<CollectionWidgetDto> toCollectionWidgetDtos);
	Widget toWidget(CollectionWidgetDto widgetDto);

	Widget convertRequestDtoToEntity(WidgetCreationRequestV1Dto widgetCreationRequestV1Dto);
	List<Widget> convertRequestDtoToEntitys(List<WidgetCreationRequestV1Dto> widgetCreationRequestV1Dtos);
	WidgetCreationResponseV1Dto convertEntityToResponseDto(Widget widget);
	List<WidgetCreationResponseV1Dto> convertEntityToResponseDtos(List<Widget> widgets);



}
