package com.licious.merchandising.dtos.mappers;

import com.licious.merchandising.dtos.models.HomePageVariantUserMappingDto;
import com.licious.merchandising.models.HomePageVariantUserMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface HomePageVariantUserMappingMapper {
    @Mapping(source = "homePageVariantUserMapping.city", target = "cityId")

    HomePageVariantUserMappingDto toHpVariantUserMappingDto(HomePageVariantUserMapping homePageVariantUserMapping);
    List<HomePageVariantUserMappingDto> toHomePageVariantUserMappingDtos(List<HomePageVariantUserMapping> homePageVariantUserMappings);

    @Mapping(source = "homePageVariantUserMappingDto.cityId", target = "city")

    HomePageVariantUserMapping toHomePageVariantUserMapping(HomePageVariantUserMappingDto homePageVariantUserMappingDto);
    List<HomePageVariantUserMapping> homePageVariantUserMappingDtos(List<HomePageVariantUserMappingDto> homePageVariantUserMappingDtos);

}
