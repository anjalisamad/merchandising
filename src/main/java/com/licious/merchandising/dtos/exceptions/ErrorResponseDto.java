package com.licious.merchandising.dtos.exceptions;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class ErrorResponseDto {
    private List<ErrorMetaDto> errors;
}
