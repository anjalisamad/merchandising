package com.licious.merchandising.enumerations;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ProductWidgetMappingType {
    MANUAL("MANUAL"),
    RULE_BASED("RULE_BASED");

    @Getter
    @JsonValue
    private final String value;
}
