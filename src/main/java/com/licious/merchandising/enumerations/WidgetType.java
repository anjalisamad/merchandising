package com.licious.merchandising.enumerations;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum WidgetType {
    COLLECTION("COLLECTION"),
    PRODUCT("PRODUCT"),
    PAGE("PAGE");

    @Getter
    @JsonValue
    private final String value;
}
