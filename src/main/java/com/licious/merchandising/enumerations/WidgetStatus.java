package com.licious.merchandising.enumerations;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum WidgetStatus {

    ACTIVE("ACTIVE"),
    INACTIVE("INACTIVE");

    @Getter
    @JsonValue
    private final String value;
}
