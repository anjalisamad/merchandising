package com.licious.merchandising.controllers;

import com.licious.merchandising.dtos.request.WidgetCreationRequestV1Dto;
import com.licious.merchandising.dtos.exceptions.ErrorMetaDto;
import com.licious.merchandising.dtos.response.WidgetCreationResponseV1Dto;
import com.licious.merchandising.exceptions.InvalidParameterValueException;
import com.licious.merchandising.exceptions.ResourceNotFoundException;
import com.licious.merchandising.models.Widget;
import com.licious.merchandising.services.WidgetService;
import com.licious.merchandising.utils.Constants;
import com.licious.merchandising.utils.ResponseResolver;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = Constants.Routes.WIDGET_BASE)
public class WidgetController {


    private final WidgetService widgetService;

    WidgetController(WidgetService widgetService) {
        this.widgetService = widgetService;
    }

    /**
     * Creates widget of a given type.
     * @param widgetCreationRequestV1Dto {@link WidgetCreationRequestV1Dto} - request object for creating new widget
     * @return saved widget
     **/
    @PostMapping
    public ResponseEntity createWidget(@Valid @RequestBody WidgetCreationRequestV1Dto widgetCreationRequestV1Dto) {
        ResponseResolver<WidgetCreationResponseV1Dto> responseResolver = new ResponseResolver<>();
        try {
            WidgetCreationResponseV1Dto widgetCreationResponseV1Dto =
                    widgetService.createWidget(widgetCreationRequestV1Dto);
            return responseResolver.responseResolver(HttpStatus.CREATED.value(), widgetCreationResponseV1Dto);
        } catch (ResourceNotFoundException | InvalidParameterValueException ex) {
            ErrorMetaDto meta = new ErrorMetaDto();
            meta.setStatus(String.valueOf(ex.getErrorCode()));
            meta.setCode(HttpStatus.valueOf(ex.getErrorCode()).toString());
            meta.setMessage(ex.getMessage());
            log.error(ex.getMessage());
            return responseResolver.resolveErrorResponse(ex.getErrorCode(), List.of(meta));
        } catch (Exception ex) {
            ErrorMetaDto meta = new ErrorMetaDto();
            meta.setStatus(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
            meta.setCode(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            meta.setMessage(ex.getMessage());
            log.error(ex.getMessage());
            return responseResolver.resolveErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), List.of(meta));
        }
    }

    /**
     * Creates widget of a given type.
     * @param widgetCreationRequestV1Dto {@link WidgetCreationRequestV1Dto} - request object for creating new widget
     * @return saved widget
     **/
    @PatchMapping(value = Constants.PathVariables.WIDGET_ID)
    public ResponseEntity updateWidget(@Valid @RequestBody WidgetCreationRequestV1Dto widgetCreationRequestV1Dto,
                                                              @PathVariable(name = Constants.UrlPathParameters.WIDGET_ID) final Long widgetId) {
        ResponseResolver<WidgetCreationResponseV1Dto> responseResolver = new ResponseResolver<>();
        try{
            WidgetCreationResponseV1Dto widgetCreationResponseV1Dto = widgetService.updateWidget(widgetCreationRequestV1Dto, widgetId);
            return responseResolver.responseResolver(HttpStatus.OK.value(), widgetCreationResponseV1Dto);
        }
        catch (ResourceNotFoundException | InvalidParameterValueException ex) {
            ErrorMetaDto meta = new ErrorMetaDto();
            meta.setStatus(String.valueOf(ex.getErrorCode()));
            meta.setCode(HttpStatus.valueOf(ex.getErrorCode()).toString());
            meta.setMessage(ex.getMessage());
            log.error(ex.getMessage());
            return responseResolver.resolveErrorResponse(ex.getErrorCode(), List.of(meta));
        }
        catch (Exception ex) {

            ErrorMetaDto meta = new ErrorMetaDto();
            meta.setStatus(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
            meta.setCode(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            meta.setMessage(ex.getMessage());
            log.error(ex.getMessage());
            return responseResolver.resolveErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), List.of(meta));
        }

    }

    /**
     * Finds all the widgets
     * @return list of all the saved widgets
     */
    @GetMapping
    public ResponseEntity getWidgets() {
        ResponseResolver<List<WidgetCreationResponseV1Dto>> responseResolver = new ResponseResolver<>();
        return responseResolver.responseResolver(HttpStatus.OK.value(), widgetService.getWidgets());
    }
}
