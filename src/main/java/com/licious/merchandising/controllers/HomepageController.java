package com.licious.merchandising.controllers;

import com.licious.merchandising.configs.CustomPropertiesConfiguration;
import com.licious.merchandising.dtos.exceptions.ErrorMetaDto;
import com.licious.merchandising.dtos.response.HomePageByVariantIdResponseDto;
import com.licious.merchandising.dtos.response.HomePageResponseDto;
import com.licious.merchandising.exceptions.InvalidParameterValueException;
import com.licious.merchandising.exceptions.ResourceNotFoundException;
import com.licious.merchandising.services.HomepageService;
import com.licious.merchandising.utils.BeanUtil;
import com.licious.merchandising.utils.Constants;
import com.licious.merchandising.utils.ResponseResolver;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.licious.merchandising.utils.ErrorMessages.INVALID_CITY_ID_AND_SEGMENT_VALUES;

@Slf4j
@RestController
@RequestMapping(value = Constants.Routes.HOMEPAGE_BASE)
public class HomepageController {

    private final HomepageService homepageService;

    HomepageController(HomepageService homepageService) {
        this.homepageService = homepageService;
    }

    @GetMapping
    public ResponseEntity getHomePage(@RequestParam(name = Constants.UrlPathParameters.CITY_ID, required = false) Long cityId,
                                      @RequestParam(name = Constants.UrlPathParameters.SEGMENT, required = false) String segment){
        ResponseResolver<HomePageResponseDto> responseResolver = new ResponseResolver<>();
        try{
            CustomPropertiesConfiguration customProperties = BeanUtil.getBean(CustomPropertiesConfiguration.class);
            if(cityId == null || segment.isEmpty()) {
                log.info(INVALID_CITY_ID_AND_SEGMENT_VALUES);
                cityId = customProperties.defaultLayout.getCityId();
                segment = customProperties.defaultLayout.getUserSegment();
            }
            HomePageResponseDto homePageResponseDto = homepageService.getHomepageDetails(cityId, segment);
            return responseResolver.responseResolver(HttpStatus.OK.value(), homePageResponseDto);
        }
        catch (ResourceNotFoundException | InvalidParameterValueException ex) {
            ErrorMetaDto meta = new ErrorMetaDto();
            meta.setStatus(String.valueOf(ex.getErrorCode()));
            meta.setCode(HttpStatus.valueOf(ex.getErrorCode()).toString());
            meta.setMessage(ex.getMessage());
            log.error(ex.getMessage());
            return responseResolver.resolveErrorResponse(ex.getErrorCode(), List.of(meta));
        }
        catch (Exception ex) {
            log.error(ex.getMessage());
            ErrorMetaDto meta = new ErrorMetaDto();
            meta.setStatus(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
            meta.setCode(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            meta.setMessage(ex.getMessage());
            log.error(ex.getMessage());
            return responseResolver.resolveErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), List.of(meta));
        }
    }

    @GetMapping(value = Constants.Routes.HOMEPAGE_BY_VARIANT_ID)
    public ResponseEntity getHomePageByVariantId(@PathVariable(name = Constants.UrlPathParameters.VARIANT_ID) Long variantId){
        ResponseResolver<HomePageByVariantIdResponseDto> responseResolver = new ResponseResolver<>();
        try{
            HomePageByVariantIdResponseDto homePageResponseDto = homepageService.getHomepageDetailsByVariantId(variantId);
            return responseResolver.responseResolver(HttpStatus.OK.value(), homePageResponseDto);
        }
        catch (ResourceNotFoundException | InvalidParameterValueException ex) {
            ErrorMetaDto meta = new ErrorMetaDto();
            meta.setStatus(String.valueOf(ex.getErrorCode()));
            meta.setCode(HttpStatus.valueOf(ex.getErrorCode()).toString());
            meta.setMessage(ex.getMessage());
            log.error(ex.getMessage());
            return responseResolver.resolveErrorResponse(ex.getErrorCode(), List.of(meta));
        }
        catch (Exception ex) {
            log.error(ex.getMessage());
            ErrorMetaDto meta = new ErrorMetaDto();
            meta.setStatus(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
            meta.setCode(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            meta.setMessage(ex.getMessage());
            log.error(ex.getMessage());
            return responseResolver.resolveErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), List.of(meta));
        }
    }
}
