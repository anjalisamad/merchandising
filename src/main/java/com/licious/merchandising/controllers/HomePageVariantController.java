package com.licious.merchandising.controllers;

import com.licious.merchandising.dtos.exceptions.ErrorMetaDto;
import com.licious.merchandising.dtos.request.HomePageVariantRequestV1Dto;
import com.licious.merchandising.dtos.response.HomePageVariantResponseV1Dto;
import com.licious.merchandising.exceptions.InvalidParameterValueException;
import com.licious.merchandising.exceptions.ResourceNotFoundException;
import com.licious.merchandising.services.HomePageVariantService;
import com.licious.merchandising.utils.ResponseResolver;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/api/v1/homepage/variants")
public class HomePageVariantController {

    private final HomePageVariantService homePageVariantService;

    HomePageVariantController(HomePageVariantService homePageVariantService) {
        this.homePageVariantService = homePageVariantService;
    }

    /**
     * Creates home page variant for a given list of segments and cities, with given list of widgets
     * @param homePageVariantRequestV1Dto {@link HomePageVariantRequestV1Dto} - request object for creating new widget
     * @return saved home page variant
     **/
    @PostMapping
    public ResponseEntity createHomePageVariant(@Valid @RequestBody HomePageVariantRequestV1Dto homePageVariantRequestV1Dto) {
        ResponseResolver<HomePageVariantResponseV1Dto> responseResolver = new ResponseResolver<>();
        try {
            HomePageVariantResponseV1Dto homePageVariantResponseV1Dto =
                    homePageVariantService.createHomePageVariant(homePageVariantRequestV1Dto);
            return responseResolver.responseResolver(HttpStatus.CREATED.value(), homePageVariantResponseV1Dto);
        } catch (ResourceNotFoundException | InvalidParameterValueException ex) {
            ErrorMetaDto meta = new ErrorMetaDto();
            meta.setStatus(String.valueOf(ex.getErrorCode()));
            meta.setCode(HttpStatus.valueOf(ex.getErrorCode()).toString());
            meta.setMessage(ex.getMessage());
            log.error(ex.getMessage());
            return responseResolver.resolveErrorResponse(ex.getErrorCode(), List.of(meta));
        } catch (Exception ex) {
            ErrorMetaDto meta = new ErrorMetaDto();
            meta.setStatus(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
            meta.setCode(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            meta.setMessage(ex.getMessage());
            log.error(ex.getMessage());
            return responseResolver.resolveErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), List.of(meta));
        }
    }

//    /**
//     * Creates widget of a given type.
//     * @param widgetCreationRequestV1Dto {@link WidgetCreationRequestV1Dto} - request object for creating new widget
//     * @return saved widget
//     **/
//    @PatchMapping(value = Constants.PathVariables.WIDGET_ID)
//    public ResponseEntity updateWidget(@Valid @RequestBody WidgetCreationRequestV1Dto widgetCreationRequestV1Dto,
//                                                              @PathVariable(name = Constants.UrlPathParameters.WIDGET_ID) final Long widgetId) {
//        ResponseResolver<WidgetCreationResponseV1Dto> responseResolver = new ResponseResolver<>();
//        try{
//            WidgetCreationResponseV1Dto widgetCreationResponseV1Dto = widgetService.updateWidget(widgetCreationRequestV1Dto, widgetId);
//            return responseResolver.responseResolver(HttpStatus.CREATED.value(), widgetCreationResponseV1Dto);
//        }
//        catch (ResourceNotFoundException | InvalidParameterValueException ex) {
//            ErrorMetaDto meta = new ErrorMetaDto();
//            meta.setStatus(String.valueOf(ex.getErrorCode()));
//            meta.setCode(HttpStatus.valueOf(ex.getErrorCode()).toString());
//            meta.setMessage(ex.getMessage());
//            log.error(ex.getMessage());
//            return responseResolver.resolveErrorResponse(ex.getErrorCode(), List.of(meta));
//        }
//        catch (Exception ex) {
//
//            ErrorMetaDto meta = new ErrorMetaDto();
//            meta.setStatus(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
//            meta.setCode(HttpStatus.INTERNAL_SERVER_ERROR.toString());
//            meta.setMessage(ex.getMessage());
//            log.error(ex.getMessage());
//            return responseResolver.resolveErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), List.of(meta));
//        }
//
//    }
}
