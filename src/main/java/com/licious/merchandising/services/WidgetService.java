package com.licious.merchandising.services;

import com.licious.merchandising.dtos.request.WidgetCreationRequestV1Dto;
import com.licious.merchandising.dtos.response.WidgetCreationResponseV1Dto;
import com.licious.merchandising.models.Widget;
import org.springframework.lang.NonNull;

import java.util.List;

public interface WidgetService {
    public WidgetCreationResponseV1Dto updateWidget(@NonNull WidgetCreationRequestV1Dto widgetDto,
                                                    @NonNull Long widgetId) throws Exception;
    public WidgetCreationResponseV1Dto createWidget(@NonNull WidgetCreationRequestV1Dto widgetCreationRequestV1Dto);

    public List<WidgetCreationResponseV1Dto> getWidgets();
}
