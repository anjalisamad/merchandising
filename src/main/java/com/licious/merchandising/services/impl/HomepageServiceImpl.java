package com.licious.merchandising.services.impl;

import com.licious.merchandising.dtos.mappers.WidgetMapper;
import com.licious.merchandising.dtos.models.WidgetMappingDto;
import com.licious.merchandising.dtos.response.*;
import com.licious.merchandising.enumerations.ProductWidgetMappingType;
import com.licious.merchandising.enumerations.WidgetType;
import com.licious.merchandising.exceptions.MerchandisingException;
import com.licious.merchandising.exceptions.ResourceNotFoundException;
import com.licious.merchandising.models.*;
import com.licious.merchandising.repositories.readonly.HomePageVariantReadOnlyRepository;
import com.licious.merchandising.repositories.readonly.HomePageVariantUserMappingReadOnlyRepository;
import com.licious.merchandising.repositories.readonly.HomePageVariantWidgetMappingReadOnlyRepository;
import com.licious.merchandising.services.HomepageService;
import com.licious.merchandising.utils.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Collections;
import java.util.Comparator;
import static com.licious.merchandising.utils.ErrorMessages.*;

@Service
@Slf4j
public class HomepageServiceImpl implements HomepageService {
    private final HomePageVariantUserMappingReadOnlyRepository homePageVariantUserMappingRepository;
    private final HomePageVariantWidgetMappingReadOnlyRepository homePageVariantWidgetMappingRepository;
    private final HomePageVariantReadOnlyRepository homePageVariantRepository;
    private final WidgetMapper widgetMapper;

    private static final String LOG_PREFIX = "HomepageServiceImpl :: ";
    private static final String WIDGET_MAPPING_ACTIVE_STATUS = "1";

    HomepageServiceImpl(HomePageVariantUserMappingReadOnlyRepository homePageVariantUserMappingRepository,
                        HomePageVariantWidgetMappingReadOnlyRepository homePageVariantWidgetMappingRepository,
                        HomePageVariantReadOnlyRepository homePageVariantRepository,
                        WidgetMapper widgetMapper) {
        this.homePageVariantUserMappingRepository = homePageVariantUserMappingRepository;
        this.homePageVariantWidgetMappingRepository = homePageVariantWidgetMappingRepository;
        this.homePageVariantRepository = homePageVariantRepository;
        this.widgetMapper = widgetMapper;
    }

    @Override
    @Cacheable(value= Constants.CacheConstants.HOMEPAGE_CACHE_KEY, key="{#cityId, #userSegment}")
    public HomePageResponseDto getHomepageDetails(Long cityId, String userSegment) {
        log.info(getLogMessage(String.format("Fetching Home Page details for cityId: %s " +
                "and user segment: %s", cityId, userSegment)));
        HomePageVariantUserMapping userMapping = homePageVariantUserMappingRepository.
                findMappingsByCityIdAndSegment(cityId, userSegment)
                .orElseThrow(() -> new ResourceNotFoundException(
                        getLogMessage(String.format(VARIANT_USER_MAPPING_NOT_FOUND, cityId, userSegment)),
                        404,
                        HttpStatus.NOT_FOUND));
        HomePageVariant variant = userMapping.getHomePageVariant();
        List<HomePageVariantWidgetMapping> variantWidgetMappings =
                homePageVariantWidgetMappingRepository.findByVariantId(variant.getId());
        if(variantWidgetMappings.isEmpty()){
            throw new MerchandisingException(getLogMessage(String.format(VARIANT_WIDGET_MAPPING_NOT_FOUND, cityId,
                    userMapping, variant.getId())));
        }

        return getHomepageResponse(userMapping, variantWidgetMappings);
    }

    @Override
    public HomePageByVariantIdResponseDto getHomepageDetailsByVariantId(Long variantId) {
        log.info(getLogMessage(String.format("Fetching Home Page details for variantId: %s", variantId)));
        HomePageVariant variant = homePageVariantRepository.findById(variantId)
                .orElseThrow(() -> new ResourceNotFoundException(
                        getLogMessage(String.format(VARIANT_NOT_FOUND, variantId)),
                        404,
                        HttpStatus.NOT_FOUND));

        List<HomePageVariantWidgetMapping> variantWidgetMappings =
                homePageVariantWidgetMappingRepository.findByVariantId(variant.getId());
        if(variantWidgetMappings.isEmpty()){
            throw new MerchandisingException(getLogMessage(String.format(WIDGET_MAPPINGS_FOR_VARIANT_NOT_FOUND,
                     variant.getId())));
        }

        return getHomepageResponseForPreview(variant, variantWidgetMappings);

    }

    private List<HomepageWidgetsDto> getHomepageWidgetDtoList(List<HomePageVariantWidgetMapping> mappings) {
        List<HomepageWidgetsDto> widgetsDtos = new ArrayList<>();
        for(HomePageVariantWidgetMapping mapping: mappings) {
            Widget widget = mapping.getWidget();
            Set<WidgetMapping> widgetMappings = widget.getWidgetMappings();
            HomepageWidgetsDto widgetDto;
            if(widget.getType().equals(WidgetType.PRODUCT.getValue())){
                if(widgetMappings.size()!=1){
                    throw new MerchandisingException(getLogMessage(String.format(INVALID_WIDGET_MAPPINGS, widget.getId())));
                }
                WidgetMapping widgetMapping = widgetMappings.stream().findFirst()
                        .orElseThrow(() -> new MerchandisingException(
                                getLogMessage(String.format(INVALID_WIDGET_MAPPINGS, widget.getId()))));
                widgetDto = getWidgetsDto(widget, mapping);
                if(widget.getMappingType().equals(ProductWidgetMappingType.MANUAL.getValue())){
                    widgetDto.setProductCollectionId(widgetMapping.getProductCollectionId());
                }
                else if (widget.getMappingType().equals(ProductWidgetMappingType.RULE_BASED.getValue())){
                    widgetDto.setRuleId(widgetMapping.getRuleId());
                }
                widgetDto.setWidgetMappings(Collections.emptyList());
            }
            else {
                WidgetCreationResponseV1Dto widgetResponseDto = widgetMapper.convertEntityToResponseDto(widget);
                List<WidgetMappingDto> mappingDtos = widgetResponseDto.getWidgetMappings();
                widgetDto = getWidgetsDto(widget, mapping);
                widgetDto.setWidgetMappings(mappingDtos);
            }
            widgetsDtos.add(widgetDto);
        }
        return widgetsDtos.stream()
                .sorted(Comparator.comparing(HomepageWidgetsDto::getOrder)).toList();
    }

    private HomepageWidgetsDto getWidgetsDto(Widget widget, HomePageVariantWidgetMapping mapping) {
        HomepageWidgetsDto dto = new HomepageWidgetsDto();
        dto.setId(widget.getId());
        dto.setAndroidEnabled(widget.getAndroidEnabled());
        dto.setIosEnabled(widget.getIosEnabled());
        dto.setWebEnabled(widget.getWebEnabled());
        dto.setMsiteEnabled(widget.getMsiteEnabled());
        dto.setOrder(Long.valueOf(mapping.getPosition()));
        dto.setName(widget.getName());
        dto.setLayoutName(widget.getLayoutName());
        dto.setLayoutAspectRatio(widget.getLayoutAspectRatio());
        dto.setType(widget.getType());
        dto.setMappingType(widget.getMappingType());
        dto.setSubText(widget.getSubText());
        dto.setAndroidEnabled(widget.getAndroidEnabled());
        dto.setStatus(widget.getStatus());

        return dto;
    }

    private HomePageResponseDto getHomepageResponse(HomePageVariantUserMapping userMapping,
                                                    List<HomePageVariantWidgetMapping> widgetMappings){
        HomePageResponseDto dto = new HomePageResponseDto();
        dto.setCity(userMapping.getCity());
        dto.setSegment(userMapping.getSegment());
        dto.setName(userMapping.getHomePageVariant().getName());
        dto.setStatus(userMapping.getHomePageVariant().getStatus());
        dto.setId(userMapping.getHomePageVariant().getId());
        List<HomePageVariantWidgetMapping> activeWidgetMappings = widgetMappings.stream().filter(
                mapping -> mapping.getStatus().equals(WIDGET_MAPPING_ACTIVE_STATUS)).toList();
        dto.setWidgets(getHomepageWidgetDtoList(activeWidgetMappings));

        return dto;
    }

    private HomePageByVariantIdResponseDto getHomepageResponseForPreview(HomePageVariant variant,
                                                    List<HomePageVariantWidgetMapping> widgetMappings){
        HomePageByVariantIdResponseDto dto = new HomePageByVariantIdResponseDto();
        Set<HomePageVariantUserMapping> userMappings = variant.getHomePageVariantUserMappings();
        List<HomePageUserMappingDto> userMappingDtos = new ArrayList<>();
        for(HomePageVariantUserMapping mapping: userMappings){
            userMappingDtos.add(HomePageUserMappingDto.builder()
                    .id(mapping.getId())
                    .city(mapping.getCity())
                    .segment(mapping.getSegment()).build());
        }
        dto.setUserMappings(userMappingDtos);
        dto.setId(variant.getId());
        dto.setName(variant.getName());
        dto.setStatus(variant.getStatus());
        List<HomePageVariantWidgetMapping> activeWidgetMappings = widgetMappings.stream().filter(
                mapping -> mapping.getStatus().equals(WIDGET_MAPPING_ACTIVE_STATUS)).toList();
        dto.setWidgets(getHomepageWidgetDtoList(activeWidgetMappings));

        return dto;
    }

    private String getLogMessage(String message) {
        return LOG_PREFIX + message;
    }
}
