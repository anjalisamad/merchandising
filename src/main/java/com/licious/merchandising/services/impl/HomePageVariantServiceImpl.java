package com.licious.merchandising.services.impl;

import com.licious.merchandising.dtos.mappers.HomePageVariantMapper;
import com.licious.merchandising.dtos.request.HomePageVariantRequestV1Dto;
import com.licious.merchandising.dtos.response.HomePageVariantResponseV1Dto;
import com.licious.merchandising.models.HomePageVariant;
import com.licious.merchandising.repositories.HomePageVariantRepository;
import com.licious.merchandising.services.HomePageVariantService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class HomePageVariantServiceImpl implements HomePageVariantService {

    private final HomePageVariantRepository homePageVariantRepository;
    private final HomePageVariantMapper homePageVariantMapper;

    HomePageVariantServiceImpl(final HomePageVariantRepository homePageVariantRepository,
                               final HomePageVariantMapper homePageVariantMapper) {
        this.homePageVariantRepository = homePageVariantRepository;
        this.homePageVariantMapper = homePageVariantMapper;
    }

    @Override
    public HomePageVariantResponseV1Dto createHomePageVariant(@NonNull final HomePageVariantRequestV1Dto homePageVariantRequestV1Dto) {
        //Add validations
        this.validateRequestDtoForCreate(homePageVariantRequestV1Dto);

        //write dto conversions to entity
        HomePageVariant homePageVariant = this.getHomePageVariantFromHomePageVariantRequestV1Dto(homePageVariantRequestV1Dto);

        //write data to db
        homePageVariant.getHomePageVariantUserMappings().forEach(userMapping -> userMapping.setHomePageVariant(homePageVariant));
        homePageVariant.getHomePageVariantWidgetMappings().forEach(widgetMapping -> widgetMapping.setHomePageVariant(homePageVariant));
        HomePageVariant updatedHomePageVariant = homePageVariantRepository.save(homePageVariant);

        //write response dto
        return homePageVariantMapper.convertEntityToResponseDto(updatedHomePageVariant);
//        return null;



//        this.validateRequestDtoForCreate(widgetCreationRequestV1Dto);
//        Widget newWidget = this.getWidgetFromWidgetCreationRequestV1Dto(widgetCreationRequestV1Dto);
//        newWidget.getWidgetMappings().forEach(widgetMapping -> widgetMapping.setWidget(newWidget));
//        Widget updatedWidget = widgetRepository.save(newWidget);
//        return widgetMapper.convertEntityToResponseDto(updatedWidget);
    }

    private HomePageVariant getHomePageVariantFromHomePageVariantRequestV1Dto(HomePageVariantRequestV1Dto homePageVariantRequestV1Dto) {
        return homePageVariantMapper.convertRequestDtoToEntity(homePageVariantRequestV1Dto);

//        if (widgetType.equals(WidgetType.PRODUCT)) {
//            this.validateRequestDtoForProductWidget(widgetCreationRequestV1Dto);
//            return homePageVariantMapper.convertRequestDtoToEntity(widgetCreationRequestV1Dto);
//        } else {
//            throw new InvalidParameterValueException(getLogMessage(String.format(INVALID_WIDGET_TYPE, widgetCreationRequestV1Dto.getType())));
//        }
    }

    private void validateRequestDtoForCreate(HomePageVariantRequestV1Dto homePageVariantRequestV1Dto) {
    }
}
