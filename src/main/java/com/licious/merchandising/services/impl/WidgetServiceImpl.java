package com.licious.merchandising.services.impl;

import com.licious.merchandising.dtos.mappers.CollectionWidgetMapper;
import com.licious.merchandising.dtos.mappers.PageWidgetMapper;
import com.licious.merchandising.dtos.mappers.ProductWidgetMapper;
import com.licious.merchandising.dtos.mappers.WidgetMapper;
import com.licious.merchandising.dtos.models.WidgetMappingDto;
import com.licious.merchandising.dtos.request.WidgetCreationRequestV1Dto;
import com.licious.merchandising.dtos.response.WidgetCreationResponseV1Dto;
import com.licious.merchandising.enumerations.ProductWidgetMappingType;
import com.licious.merchandising.enumerations.WidgetType;
import com.licious.merchandising.exceptions.InvalidParameterValueException;
import com.licious.merchandising.exceptions.ResourceNotFoundException;
import com.licious.merchandising.models.Widget;
import com.licious.merchandising.repositories.WidgetRepository;
import com.licious.merchandising.services.WidgetService;
import com.licious.merchandising.utils.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

import static com.licious.merchandising.utils.ErrorMessages.*;

@Service
@Slf4j
public class WidgetServiceImpl implements WidgetService {

    private final WidgetRepository widgetRepository;
    private final CollectionWidgetMapper collectionWidgetMapper;
    private final PageWidgetMapper pageWidgetMapper;
    private final ProductWidgetMapper productWidgetMapper;
    private final WidgetMapper widgetMapper;
    private static final String LOG_PREFIX = "WidgetServiceImpl :: ";

    public WidgetServiceImpl(final WidgetRepository widgetRepository,
                      final CollectionWidgetMapper collectionWidgetMapper,
                      final PageWidgetMapper pageWidgetMapper,
                      final ProductWidgetMapper productWidgetMapper,
                      final WidgetMapper widgetMapper) {
        this.widgetRepository = widgetRepository;
        this.collectionWidgetMapper = collectionWidgetMapper;
        this.pageWidgetMapper = pageWidgetMapper;
        this.productWidgetMapper = productWidgetMapper;
        this.widgetMapper = widgetMapper;
    }

    @Override
    public WidgetCreationResponseV1Dto createWidget(@NonNull final WidgetCreationRequestV1Dto widgetCreationRequestV1Dto) {
        this.validateRequestDtoForCreate(widgetCreationRequestV1Dto);
        Widget newWidget = this.getWidgetFromWidgetCreationRequestV1Dto(widgetCreationRequestV1Dto);
        newWidget.getWidgetMappings().forEach(widgetMapping -> widgetMapping.setWidget(newWidget));
        Widget updatedWidget = widgetRepository.save(newWidget);
        return widgetMapper.convertEntityToResponseDto(updatedWidget);
    }

    @Override
    @CacheEvict(value = Constants.CacheConstants.HOMEPAGE_CACHE_KEY, allEntries = true)
    public WidgetCreationResponseV1Dto updateWidget(@NonNull WidgetCreationRequestV1Dto widgetDto,
                                                    @NonNull Long widgetId) throws Exception {
        this.validateRequestDtoForUpdate(widgetDto, widgetId);
        Widget updatedWidget = this.getWidgetFromWidgetCreationRequestV1Dto(widgetDto);
        updatedWidget.getWidgetMappings().forEach(mapping -> mapping.setWidget(updatedWidget));
        Widget finalUpdatedWidget = widgetRepository.findById(widgetDto.getId())
                .map(widget -> widgetRepository.save(updatedWidget))
                .orElseThrow(() -> new ResourceNotFoundException(
                        getLogMessage(String.format(WIDGET_NOT_FOUND, widgetDto.getId())),
                        404,
                        HttpStatus.NOT_FOUND)
                );
        return widgetMapper.convertEntityToResponseDto(finalUpdatedWidget);
    }

    private String getLogMessage(String message) {
        return LOG_PREFIX + message;
    }

    private Widget getWidgetFromWidgetCreationRequestV1Dto(WidgetCreationRequestV1Dto widgetCreationRequestV1Dto) {
        WidgetType widgetType = WidgetType.valueOf(widgetCreationRequestV1Dto.getType());
        if (widgetType.equals(WidgetType.COLLECTION)) {
            this.validateRequestDtoForCollectionWidget(widgetCreationRequestV1Dto);
            return collectionWidgetMapper.convertRequestDtoToEntity(widgetCreationRequestV1Dto);
        } else if (widgetType.equals(WidgetType.PAGE)) {
            this.validateRequestDtoForPageWidget(widgetCreationRequestV1Dto);
            return pageWidgetMapper.convertRequestDtoToEntity(widgetCreationRequestV1Dto);
        } else if (widgetType.equals(WidgetType.PRODUCT)) {
            this.validateRequestDtoForProductWidget(widgetCreationRequestV1Dto);
            return productWidgetMapper.convertRequestDtoToEntity(widgetCreationRequestV1Dto);
        } else {
            throw new InvalidParameterValueException(getLogMessage(String.format(INVALID_WIDGET_TYPE, widgetCreationRequestV1Dto.getType())));
        }
    }

    private void validateRequestDtoForUpdate(WidgetCreationRequestV1Dto dto, Long widgetId) throws RuntimeException{
        if(dto.getId() == null){
            throw new InvalidParameterValueException(WIDGET_ID_NULL);
        }
        if(!Objects.equals(dto.getId(), widgetId)){
            throw new InvalidParameterValueException(WIDGET_ID_MISMATCH);
        }
    }

    private void validateRequestDtoForPageWidget(WidgetCreationRequestV1Dto dto) throws RuntimeException {
        if (dto.getMappingType() != null) {
            throw new InvalidParameterValueException(MAPPING_TYPE_NOT_NULL);
        }

        List<WidgetMappingDto> widgetMappings = dto.getWidgetMappings();
        if(widgetMappings.size() != 0){
            widgetMappings.forEach(widgetMappingDto -> {
                if (widgetMappingDto.getCategoryId() != null) {
                    throw new InvalidParameterValueException(CATEGORY_ID_NOT_NULL);
                }
                if (widgetMappingDto.getProductCollectionId() != null) {
                    throw new InvalidParameterValueException(PRODUCT_COLLECTION_ID_NOT_NULL);
                }
                if (widgetMappingDto.getRuleId() != null) {
                    throw new InvalidParameterValueException(RULE_ID_NOT_NULL);
                }

                if (widgetMappingDto.getWebUrl() == null) {
                    throw new InvalidParameterValueException(WEB_URL_NULL);
                }
                if (widgetMappingDto.getAppUrl() == null) {
                    throw new InvalidParameterValueException(APP_URL_NULL);
                }
                if (widgetMappingDto.getAppImage() == null) {
                    throw new InvalidParameterValueException(APP_IMAGE_NULL);
                }
                if (widgetMappingDto.getWebImage() == null) {
                    throw new InvalidParameterValueException(WEB_IMAGE_NULL);
                }
                if (widgetMappingDto.getMsiteImage() == null) {
                    throw new InvalidParameterValueException(MSITE_IMAGE_NULL);
                }
                if (widgetMappingDto.getCardName() == null) {
                    throw new InvalidParameterValueException(CARD_NAME_NULL);
                }
                if (widgetMappingDto.getCardPosition() == null) {
                    throw new InvalidParameterValueException(CARD_POSITION_NULL);
                }
            });
        }
    }

    private void validateRequestDtoForProductWidget(WidgetCreationRequestV1Dto dto) throws RuntimeException {
        List<WidgetMappingDto> widgetMappings = dto.getWidgetMappings();
        if(widgetMappings.size() == 1){
            widgetMappings.forEach(widgetMappingDto -> {
                if (widgetMappingDto.getCategoryId() != null) {
                    throw new InvalidParameterValueException(CATEGORY_ID_NOT_NULL);
                }
                if (widgetMappingDto.getWebUrl() != null) {
                    throw new InvalidParameterValueException(WEB_URL_NOT_NULL);
                }
                if (widgetMappingDto.getAppUrl() != null) {
                    throw new InvalidParameterValueException(APP_URL_NOT_NULL);
                }
                if (widgetMappingDto.getAppImage() != null) {
                    throw new InvalidParameterValueException(APP_IMAGE_NOT_NULL);
                }
                if (widgetMappingDto.getWebImage() != null) {
                    throw new InvalidParameterValueException(WEB_IMAGE_NOT_NULL);
                }
                if (widgetMappingDto.getMsiteImage() != null) {
                    throw new InvalidParameterValueException(MSITE_IMAGE_NOT_NULL);
                }
                if (widgetMappingDto.getCardName() != null) {
                    throw new InvalidParameterValueException(CARD_NAME_NOT_NULL);
                }
                if (widgetMappingDto.getCardPosition() != null) {
                    throw new InvalidParameterValueException(CARD_POSITION_NOT_NULL);
                }

                if (dto.getMappingType().equals(ProductWidgetMappingType.MANUAL.getValue()) && widgetMappingDto.getProductCollectionId() == null) {
                    throw new InvalidParameterValueException(PRODUCT_COLLECTION_ID_NULL);
                } else if (dto.getMappingType().equals(ProductWidgetMappingType.RULE_BASED.getValue()) && widgetMappingDto.getRuleId() == null) {
                    throw new InvalidParameterValueException(RULE_ID_NULL);
                } else if (dto.getMappingType().equals(ProductWidgetMappingType.MANUAL.getValue()) && widgetMappingDto.getRuleId() != null) {
                    throw new InvalidParameterValueException(RULE_ID_MAPPING_TYPE_MISMATCH);
                } else if (dto.getMappingType().equals(ProductWidgetMappingType.RULE_BASED.getValue()) && widgetMappingDto.getProductCollectionId() != null) {
                    throw new InvalidParameterValueException(PRODUCT_COLLECTION_ID_MAPPING_TYPE_MISMATCH);
                }
            });
        } else if (widgetMappings.size() > 1) {
            throw new InvalidParameterValueException(PRODUCT_WIDGET_MAPPING_MISMATCH);
        }
    }

    private void validateRequestDtoForCollectionWidget(WidgetCreationRequestV1Dto dto) throws RuntimeException {
        if (dto.getMappingType() != null) {
            throw new InvalidParameterValueException(MAPPING_TYPE_NOT_NULL);
        }

        List<WidgetMappingDto> widgetMappings = dto.getWidgetMappings();
        if(widgetMappings.size() != 0){
            widgetMappings.forEach(widgetMappingDto -> {
                if (widgetMappingDto.getProductCollectionId() != null) {
                    throw new InvalidParameterValueException(PRODUCT_COLLECTION_ID_NOT_NULL);
                }
                if (widgetMappingDto.getRuleId() != null) {
                    throw new InvalidParameterValueException(RULE_ID_NOT_NULL);
                }

                if (widgetMappingDto.getCategoryId() == null) {
                    throw new InvalidParameterValueException(CATEGORY_ID_NULL);
                }
                if (widgetMappingDto.getWebUrl() == null) {
                    throw new InvalidParameterValueException(WEB_URL_NULL);
                }
                if (widgetMappingDto.getAppUrl() == null) {
                    throw new InvalidParameterValueException(APP_URL_NULL);
                }
                if (widgetMappingDto.getAppImage() == null) {
                    throw new InvalidParameterValueException(APP_IMAGE_NULL);
                }
                if (widgetMappingDto.getWebImage() == null) {
                    throw new InvalidParameterValueException(WEB_IMAGE_NULL);
                }
                if (widgetMappingDto.getMsiteImage() == null) {
                    throw new InvalidParameterValueException(MSITE_IMAGE_NULL);
                }
                if (widgetMappingDto.getCardName() == null) {
                    throw new InvalidParameterValueException(CARD_NAME_NULL);
                }
                if (widgetMappingDto.getCardPosition() == null) {
                    throw new InvalidParameterValueException(CARD_POSITION_NULL);
                }
            });
        }
    }

    private void validateRequestDtoForCreate(WidgetCreationRequestV1Dto dto) throws RuntimeException {
        if(dto.getId() != null){
            throw new InvalidParameterValueException(WIDGET_ID_NOT_NULL);
        }
    }

    @Override
    public List<WidgetCreationResponseV1Dto> getWidgets() {
        return widgetMapper.convertEntityToResponseDtos(widgetRepository.findAll());
    }
}
