package com.licious.merchandising.services;

import com.licious.merchandising.dtos.request.HomePageVariantRequestV1Dto;
import com.licious.merchandising.dtos.response.HomePageVariantResponseV1Dto;
import org.springframework.lang.NonNull;

public interface HomePageVariantService {
    public HomePageVariantResponseV1Dto createHomePageVariant(@NonNull HomePageVariantRequestV1Dto homePageVariantRequestV1Dto);
}
