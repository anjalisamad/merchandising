package com.licious.merchandising.services;

import com.licious.merchandising.dtos.response.HomePageByVariantIdResponseDto;
import com.licious.merchandising.dtos.response.HomePageResponseDto;

public interface HomepageService {
    public HomePageResponseDto getHomepageDetails(Long cityId, String userSegment);
    public HomePageByVariantIdResponseDto getHomepageDetailsByVariantId(Long variantId);
}
