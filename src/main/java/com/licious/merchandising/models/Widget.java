package com.licious.merchandising.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Set;

@Setter
@Getter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "widget")
@Table(name = "widget")
public class Widget extends BaseModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    protected Long id;

    @Column(name = "name", nullable = false)
    protected String name;

    @Column(name = "type", nullable = false)
    protected String type;

    @Column(name = "sub_text", nullable = false)
    protected String subText;

    @Column(name = "ios_enabled", nullable = false)
    protected Boolean iosEnabled;

    @Column(name = "android_enabled", nullable = false)
    protected Boolean androidEnabled;

    @Column(name = "web_enabled", nullable = false)
    protected Boolean webEnabled;

    @Column(name = "msite_enabled", nullable = false)
    protected Boolean msiteEnabled;

    @Column(name = "layout", nullable = false)
    protected String layoutName;

    @Column(name = "layout_aspect_ratio", nullable = false)
    protected String layoutAspectRatio;

    @Column(name = "status", nullable = false)
    protected String status;

    @Column(name = "mapping_type")
    protected String mappingType;

    @OneToMany(mappedBy = "widget", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    protected Set<HomePageVariantWidgetMapping> homePageVariantWidgetMappings;

    @OneToMany(mappedBy = "widget", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.EAGER)
    protected Set<WidgetMapping> widgetMappings;
}