package com.licious.merchandising.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Set;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Entity
@Table(name = "home_page_variant")
public class HomePageVariant extends BaseModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    protected Long id;

    @Column(name = "name", nullable = false)
    protected String name;

    @Column(name = "status", nullable = false)
    protected String status;

    @OneToMany(mappedBy = "homePageVariant", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.EAGER)
    protected Set<HomePageVariantUserMapping> homePageVariantUserMappings;

    @OneToMany(mappedBy = "homePageVariant", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    protected Set<HomePageVariantWidgetMapping> homePageVariantWidgetMappings;
}
