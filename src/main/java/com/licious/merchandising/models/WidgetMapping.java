package com.licious.merchandising.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Setter
@Getter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "widget_mapping")
@Table(name = "widget_mapping")
public class WidgetMapping extends BaseModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    protected Long id;

    @Column(name = "card_name")
    protected String cardName;

    @Column(name = "card_position")
    protected Integer cardPosition;

    @Column(name = "web_image")
    protected String webImage;

    @Column(name = "msite_image")
    protected String msiteImage;

    @Column(name = "app_image")
    protected String appImage;

    @Column(name = "product_collection_id")
    protected Long productCollectionId;

    @Column(name = "rule_id")
    protected Long ruleId;

    @Column(name = "web_url")
    protected String webUrl;

    @Column(name = "app_url")
    protected String appUrl;

    @Column(name = "category_id")
    protected Long categoryId;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "widget_id", referencedColumnName = "id", nullable = false)
    @JsonIgnore
    protected Widget widget;
}
