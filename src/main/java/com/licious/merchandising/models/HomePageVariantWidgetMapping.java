package com.licious.merchandising.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@SuperBuilder
@Table(name = "home_page_variant_widget_mapping")
public class HomePageVariantWidgetMapping extends BaseModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    protected Long id;

    @Column(name = "variant_id", insertable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long variantId;

    @Column(name = "widget_id", insertable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long widgetId;

    @Column(name = "position", nullable = false)
    protected Integer position;

    @Column(name = "status", nullable = false)
    protected String status;

    @ManyToOne
    @JoinColumn(name = "widget_id", nullable = false)
    Widget widget;

    @ManyToOne
    @JoinColumn(name = "variant_id", nullable = false)
    HomePageVariant homePageVariant;
}
