package com.licious.merchandising;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MerchandisingApplication {

	public static void main(String[] args) {

		SpringApplication.run(MerchandisingApplication.class, args);
	}

}
