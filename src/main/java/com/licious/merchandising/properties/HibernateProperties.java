package com.licious.merchandising.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.lang.NonNull;

import java.util.Properties;

@Getter
@Setter
public class HibernateProperties {
    private String ddlAuto = "validate";
    private Boolean showSql = true;
    private String defaultSchema = "catalogue";
    private Integer jdbcFetchSize = 250;
    private Boolean generateStatistics = true;
    private Integer batchSize = 300;
    private Boolean orderInserts = true;
    private String dialect = "org.hibernate.dialect.MySQL8Dialect";

    public void generateProperties(@NonNull Properties jpaProperties) {
        jpaProperties.put("hibernate.show_sql", getShowSql());
        jpaProperties.put("hibernate.generate_statistics", getGenerateStatistics());
        jpaProperties.put("hibernate.order_inserts", getOrderInserts());
        jpaProperties.put("hibernate.hbm2ddl.auto", getDdlAuto());
        jpaProperties.put("hibernate.jdbc.batch_size", getBatchSize());
        jpaProperties.put("hibernate.jdbc.fetch_size", getJdbcFetchSize());
        jpaProperties.put("hibernate.dialect", getDialect());


        if (getDefaultSchema() != null) {
            jpaProperties.put("hibernate.default_schema", getDefaultSchema());
        }
    }
}
