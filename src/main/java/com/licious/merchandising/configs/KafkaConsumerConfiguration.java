package com.licious.merchandising.configs;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.TopicPartition;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.listener.CommonErrorHandler;
import org.springframework.kafka.listener.DeadLetterPublishingRecoverer;
import org.springframework.kafka.listener.DefaultErrorHandler;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.util.backoff.FixedBackOff;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@EnableKafka
@EnableRetry
@Configuration
@RequiredArgsConstructor
public class KafkaConsumerConfiguration {

    private static final String RETRY_SUFFIX = "-retry";
    private static final String ERROR_SUFFIX = "-error";
    private static final int RETRY_BACKOFF_INTERVAL = 3000; //millis
    private static final int MAX_RETRY = 2;
    private final KafkaTemplate<String, Object> kafkaTemplate;
    @Value("${spring.kafka.bootstrap.servers}")
    private String bootstrapServers;
    @Value("${spring.kafka.consumer.group-id}")
    private String groupId;
    @Value("${spring.kafka.consumer.properties.max.poll.interval.ms}")
    private int pollInterval;

    private Map<String, Object> getCommonConfig(){
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        props.put(ConsumerConfig.MAX_POLL_INTERVAL_MS_CONFIG, pollInterval);
        return props;
    }

    private CommonErrorHandler getMainErrorHandler(){
        var recoverer = new DeadLetterPublishingRecoverer(
                kafkaTemplate,
                (cr, exception) -> {
                    String retryTopic = cr.topic() + RETRY_SUFFIX;
                    log.warn("Pushed failed message"+ cr.value() +" from topic: "+cr.topic()+" to retry queue: "+retryTopic, exception);
                    return new TopicPartition(retryTopic, cr.partition());
                }
        );
        return new DefaultErrorHandler(recoverer, new FixedBackOff(RETRY_BACKOFF_INTERVAL, 0)
        );
    }

    private CommonErrorHandler getRetryErrorHandler(final int maxRetry){
        var recoverer = new DeadLetterPublishingRecoverer(
                kafkaTemplate,
                (cr, exception) -> {
                    String errorTopic = cr.topic().replace(RETRY_SUFFIX, "") + ERROR_SUFFIX;
                    log.warn("Pushed failed message from topic: "+cr.topic()+" to error queue: "+errorTopic, exception);
                    return new TopicPartition(errorTopic, cr.partition());
                }
        );
        return new DefaultErrorHandler(recoverer, new FixedBackOff(RETRY_BACKOFF_INTERVAL, maxRetry));
    }
}
