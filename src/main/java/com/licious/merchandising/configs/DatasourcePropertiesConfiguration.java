package com.licious.merchandising.configs;

import com.licious.merchandising.properties.DataSourceProperties;
import com.licious.merchandising.properties.HibernateProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DatasourcePropertiesConfiguration {

    @Bean
    @ConfigurationProperties(prefix = "merchandising.db.datasource.primary")
    public DataSourceProperties primaryDataSourceProperties(){
        return new DataSourceProperties();
    }

    @Bean
    @ConfigurationProperties(prefix = "merchandising.db.datasource.secondary")
    public DataSourceProperties secondaryDataSourceProperties(){
        return new DataSourceProperties();
    }

    @Bean
    @ConfigurationProperties(prefix = "merchandising.db.hibernate")
    public HibernateProperties hibernateProperties(){
        return new HibernateProperties();
    }
}
