package com.licious.merchandising.configs;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Component
@ConfigurationProperties("custom-properties")
@Data
public class CustomPropertiesConfiguration {

    @NotNull
    public DefaultLayout defaultLayout;

    @Data
    public static class DefaultLayout {
        @NotNull
        private Long cityId;
        @NotNull
        private String userSegment;
    }
}
