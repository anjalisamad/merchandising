package com.licious.merchandising.configs;

import com.licious.merchandising.annotations.ReadOnlyRepository;
import com.licious.merchandising.properties.DataSourceProperties;
import com.licious.merchandising.properties.HibernateProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

@Slf4j
@Configuration
@EnableJpaRepositories(
        basePackages = "com.licious.merchandising.repositories.readonly",
        includeFilters = @ComponentScan.Filter(ReadOnlyRepository.class),
        entityManagerFactoryRef = "secondaryEntityManagerFactory",
        transactionManagerRef = "secondaryTransactionManager"
)
public class SecondaryDatasourceConfiguration {

    private final DataSourceProperties dataSourceProperties;
    private final HibernateProperties hibernateProperties;

    public SecondaryDatasourceConfiguration(
            @Qualifier("secondaryDataSourceProperties") DataSourceProperties dataSourceProperties,
            @Qualifier("hibernateProperties") HibernateProperties hibernateProperties) {
        this.dataSourceProperties = dataSourceProperties;
        this.hibernateProperties = hibernateProperties;
    }

    @Primary
    @Bean(name = "secondaryDatasource")
    public DataSource dataSource() {
        return dataSourceProperties.getDataSource();
    }

    @Primary
    @Bean(name = "secondaryEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean secondaryEntityManagerFactory(
            @Qualifier("secondaryDatasource") DataSource secondaryDataSource
                                                                               ) {
        log.info("Spring Data JPA :: initializing secondaryEntityManagerFactory");
        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        entityManagerFactoryBean.setPackagesToScan("com.licious.merchandising.models");
        entityManagerFactoryBean.setDataSource(secondaryDataSource);

        Properties jpaProperties = new Properties();
        dataSourceProperties.generateProperties(jpaProperties);
        hibernateProperties.generateProperties(jpaProperties);

        entityManagerFactoryBean.setJpaProperties(jpaProperties);
        return entityManagerFactoryBean;
    }

    @Primary
    @Bean(name = "secondaryTransactionManager")
    public JpaTransactionManager secondaryTransactionManager(
            @Qualifier("secondaryEntityManagerFactory") EntityManagerFactory entityManagerFactory
                                                            ) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory);
        return transactionManager;
    }
}