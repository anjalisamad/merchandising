package com.licious.merchandising.configs;

import com.licious.merchandising.properties.DataSourceProperties;
import com.licious.merchandising.properties.HibernateProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

@Slf4j
@Configuration
@EnableJpaRepositories(
        basePackages = "com.licious.merchandising.repositories",
        includeFilters = @ComponentScan.Filter(Repository.class),
        entityManagerFactoryRef = "primaryEntityManagerFactory",
        transactionManagerRef = "primaryTransactionManager"
)
public class PrimaryDatasourceConfiguration {

    private final DataSourceProperties dataSourceProperties;
    private final HibernateProperties hibernateProperties;

    public PrimaryDatasourceConfiguration(
            @Qualifier("primaryDataSourceProperties") DataSourceProperties dataSourceProperties,
            @Qualifier("hibernateProperties") HibernateProperties hibernateProperties) {
        this.dataSourceProperties = dataSourceProperties;
        this.hibernateProperties = hibernateProperties;
    }

    @Primary
    @Bean(name = "datasource")
    public DataSource dataSource() {
        return dataSourceProperties.getDataSource();
    }

    @Primary
    @Bean(name = "primaryEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean primaryEntityManagerFactory(
            @Qualifier("datasource") DataSource primaryDataSource
                                                                             ) {
        log.info("Spring Data JPA :: initializing primaryEntityManagerFactory");
        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        entityManagerFactoryBean.setPackagesToScan("com.licious.merchandising.models");
        entityManagerFactoryBean.setDataSource(primaryDataSource);

        Properties jpaProperties = new Properties();
        dataSourceProperties.generateProperties(jpaProperties);
        hibernateProperties.generateProperties(jpaProperties);

        entityManagerFactoryBean.setJpaProperties(jpaProperties);
        return entityManagerFactoryBean;
    }

    @Primary
    @Bean(name = "primaryTransactionManager")
    public JpaTransactionManager primaryTransactionManager(
            @Qualifier("primaryEntityManagerFactory") EntityManagerFactory entityManagerFactory
                                                          ) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory);
        return transactionManager;
    }
}
